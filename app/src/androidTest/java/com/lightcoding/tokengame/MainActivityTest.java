package com.lightcoding.tokengame;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.widget.RelativeLayout;
import android.widget.TableRow;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);


    private void startGame() {
        ViewInteraction appCompatButton = onView(allOf(withId(R.id.btStart), isDisplayed()));
        appCompatButton.perform(click());
    }

    private List<TokenView> getTokenViews() {
        List<TokenView> tokenViews = new ArrayList<>();
        RelativeLayout rlBase = (RelativeLayout) mActivityTestRule.getActivity().findViewById(R.id.rlBase);
        BoardView boardView = (BoardView) rlBase.getChildAt(0);
        for (int i = 0; i < boardView.getChildCount(); i++) {
            TableRow tableRow = (TableRow) boardView.getChildAt(i);
            for (int j = 0; j < tableRow.getChildCount(); j++) {
                tokenViews.add((TokenView) tableRow.getChildAt(j));
            }
        }

        return tokenViews;
    }

    private TokenView getWinTokenView() {
        List<TokenView> tokenViews = getTokenViews();

        for (TokenView tokenView : tokenViews) {
            if (tokenView.getToken().isWinning()) {
                return tokenView;
            }
        }
        return null;
    }

    private void looseGame() {
        List<TokenView> tokenViews = getTokenViews();
        final List<TokenView> selectedTokenViews = new ArrayList<>();

        while (selectedTokenViews.size() < Config.MAX_TRY) {
            Random generator = new Random();
            TokenView tokenView = tokenViews.get(generator.nextInt(Config.BOARD_WIDTH * Config.BOARD_HEIGHT));
            if (!tokenView.getToken().isWinning() && !selectedTokenViews.contains(tokenView)) {
                selectedTokenViews.add(tokenView);
            }
        }

        try {
            mActivityTestRule.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (TokenView tokenView : selectedTokenViews) {
                        tokenView.performClick();
                    }
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Test
    public void testInitGame() {
        startGame();

        ViewInteraction textView = onView(allOf(withId(R.id.tvTimer), isDisplayed()));
        textView.check(matches(withText(mActivityTestRule.getActivity().getString(R.string.time, Config.MAX_GAME_TIME))));

        ViewInteraction textView2 = onView(allOf(withId(R.id.tvTryCount), isDisplayed()));
        textView2.check(matches(withText(mActivityTestRule.getActivity().getString(R.string.try_count, 0))));
    }

    @Test
    public void testEndTime() {
        startGame();

        try {
            Thread.sleep(Config.MAX_GAME_TIME * 1000 + 2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction message = onView(allOf(withId(android.R.id.message), isDisplayed()));
        message.check(matches(withText(R.string.end_time)));
    }


    @Test
    public void testIsOnlyOneWinToken() {
        startGame();
        List<TokenView> tokenViews = getTokenViews();

        int size = 0;

        for (TokenView tokenView : tokenViews) {
            if (tokenView.getToken().isWinning()) {
                size += 1;
            }
        }

        assertEquals(size, 1);
    }

    @Test
    public void testBoardSize() {
        startGame();

        List<TokenView> tokenViews = getTokenViews();
        assertEquals(tokenViews.size(), Config.BOARD_HEIGHT * Config.BOARD_WIDTH);
    }

    @Test
    public void testWin() {
        startGame();

        final TokenView winTokenView = getWinTokenView();
        assertNotNull(winTokenView);

        try {
            mActivityTestRule.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    winTokenView.performClick();
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        ViewInteraction message = onView(allOf(withId(android.R.id.message), isDisplayed()));
        message.check(matches(withText(R.string.win)));
    }

    @Test
    public void testLoose() {
        startGame();
        looseGame();

        ViewInteraction message = onView(allOf(withId(android.R.id.message), isDisplayed()));
        message.check(matches(withText(R.string.loose)));
    }

    @Test
    public void testUseTokenAfterEndGame() {
        startGame();
        looseGame();

        ViewInteraction buttonOk = onView(allOf(withId(android.R.id.button3), withText(R.string.ok)));
        buttonOk.perform(scrollTo(), click());

        TokenView selectedToken = null;

        List<TokenView> tokenViews = getTokenViews();
        for (TokenView tokenView : tokenViews) {
            if (tokenView.getToken().getSide() == Token.Side.COVERED) {
                selectedToken = tokenView;
                break;
            }
        }

        try {
            final TokenView finalSelectedToken = selectedToken;
            mActivityTestRule.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    finalSelectedToken.performClick();
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        assertEquals(selectedToken.getToken().getSide(), Token.Side.COVERED);


    }

}
