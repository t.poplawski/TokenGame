package com.lightcoding.tokengame;

import android.content.Context;
import android.view.View;

import java.util.Random;

/**
 * TokenGame
 * Created by Tomasz Poplawski on 06.06.2017.
 */

public class Board {

    private final TokenI tokenI;
    private int boardWidth;
    private int boardHeight;

    private Token[] tokens;

    public Board(int boardWidth, int boardHeight, TokenI tokenI) {
        this.boardWidth = boardWidth;
        this.boardHeight = boardHeight;
        this.tokenI = tokenI;
        generateTokens();
    }

    public int getBoardWidth() {
        return boardWidth;
    }

    public int getBoardHeight() {
        return boardHeight;
    }

    public Token[] getTokens() {
        return tokens;
    }

    private void generateTokens() {
        Random r = new Random();
        int winningIndex = r.nextInt(boardWidth * boardHeight);
        tokens = new Token[boardWidth * boardHeight];
        for (int i = 0; i < boardWidth * boardHeight; i++) {
            tokens[i] = new Token(i == winningIndex);
        }
    }

    public void onTokenClick(Token token) {
        tokenI.onTokenClick(token);

    }

    public View getView(Context context) {
        return new BoardView(context, this);
    }
}
