package com.lightcoding.tokengame;

import android.content.Context;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;

/**
 * TokenGame
 * Created by Tomasz Poplawski on 06.06.2017.
 */

public class BoardView extends TableLayout {

    private final Board board;

    public BoardView(Context context, Board board) {
        super(context);
        this.board = board;
        createBoard();
    }

    private void createBoard() {
        int index = 0;
        for (int i = 0; i < board.getBoardHeight(); i++) {

            TableRow tableRow = new TableRow(getContext());

            for (int j = 0; j < board.getBoardWidth(); j++) {
                TokenView tokenView = board.getTokens()[index].getView(getContext());
                tokenView.setOnClickListener(tokenClickListener);
                tableRow.addView(tokenView);
                index += 1;
            }
            this.addView(tableRow);

        }
    }

    OnClickListener tokenClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            board.onTokenClick(((TokenView) v).getToken());
        }
    };

}