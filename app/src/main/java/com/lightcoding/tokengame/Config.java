package com.lightcoding.tokengame;

/**
 * TokenGame
 * Created by Tomasz Poplawski on 07.06.2017.
 */

public class Config {

    public static final int BOARD_WIDTH = 5;
    public static final int BOARD_HEIGHT = 4;
    public static final int MAX_TRY = 5;
    public static final int MAX_GAME_TIME = 60;

}
