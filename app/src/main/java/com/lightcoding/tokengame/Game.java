package com.lightcoding.tokengame;

import android.os.Handler;
import android.os.Message;

import java.util.Timer;
import java.util.TimerTask;

/**
 * TokenGame
 * Created by Tomasz Poplawski on 06.06.2017.
 */

public class Game implements TokenI {

    private static final int MESSAGE_UPDATE_TIME = 100;
    private static final int MESSAGE_END_TIME = 101;

    private int elapsedTime;
    private int tryCount;

    private final GameI gameI;
    private Board board;
    private Timer timer;
    private Status status = Status.IDLE;

    enum Status {
        IDLE, RUNNING, END
    }

    public Game(GameI gameI) {
        this.gameI = gameI;
        this.board = new Board(Config.BOARD_WIDTH, Config.BOARD_HEIGHT, this);
    }

    @Override
    public void onTokenClick(Token token) {

        if (status != Status.RUNNING || token.getSide() == Token.Side.UNCOVERED) {
            return;
        }
        tryCount += 1;
        gameI.onChangeTryCount(tryCount);
        token.setSide(Token.Side.UNCOVERED);


        if (token.isWinning()) {
            stopGame();
            gameI.onWin();
        } else {
            if (tryCount == Config.MAX_TRY) {
                stopGame();
                gameI.onLoose();
            }
        }
    }

    private void startTimer() {
        gameI.onChangeTime(elapsedTime);

        if (timer != null) {
            timer.cancel();
        }

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (elapsedTime > 0) {
                    handler.obtainMessage(MESSAGE_UPDATE_TIME, elapsedTime).sendToTarget();
                    elapsedTime -= 1;
                } else {
                    handler.obtainMessage(MESSAGE_END_TIME).sendToTarget();
                }
            }
        }, 1000, 1000);
    }

    public void startGame() {
        tryCount = 0;
        gameI.onChangeTryCount(tryCount);
        elapsedTime = Config.MAX_GAME_TIME;
        startTimer();
        status = Status.RUNNING;

    }

    public void stopGame() {
        status = Status.END;
        if (timer != null) {
            timer.cancel();
        }
    }


    public Board getBoard() {
        return board;
    }

    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_END_TIME:
                    stopGame();
                    gameI.onEndTime();
                    break;

                case MESSAGE_UPDATE_TIME:
                    gameI.onChangeTime(elapsedTime);
                    break;
            }
        }
    };

    public Status getStatus() {
        return status;
    }

}
