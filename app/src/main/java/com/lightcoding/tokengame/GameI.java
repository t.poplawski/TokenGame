package com.lightcoding.tokengame;

/**
 * TokenGame
 * Created by Tomasz Poplawski on 06.06.2017.
 */

public interface GameI {

    void onEndTime();

    void onWin();

    void onLoose();

    void onChangeTime(int seconds);

    void onChangeTryCount(int tryCount);
}
