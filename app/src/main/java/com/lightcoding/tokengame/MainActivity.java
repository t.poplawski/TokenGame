package com.lightcoding.tokengame;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements GameI {

    private TextView tvTimer, tvTryCount;
    private RelativeLayout rlBase;
    private Game game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rlBase = (RelativeLayout) findViewById(R.id.rlBase);
        tvTimer = (TextView) findViewById(R.id.tvTimer);
        tvTryCount = (TextView) findViewById(R.id.tvTryCount);
        Button btStart = (Button) findViewById(R.id.btStart);
        btStart.setOnClickListener(btStartClickListener);


    }


    View.OnClickListener btStartClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (game != null) {
                game.stopGame();
            }
            game = new Game(MainActivity.this);
            rlBase.removeAllViews();
            rlBase.addView(game.getBoard().getView(MainActivity.this));
            game.startGame();
        }
    };

    @Override
    public void onEndTime() {
        showAlert(R.string.end_time);
    }

    @Override
    public void onWin() {
        showAlert(R.string.win);
    }

    @Override
    public void onLoose() {
        showAlert(R.string.loose);
    }

    @Override
    public void onChangeTime(int seconds) {
        tvTimer.setText(getString((R.string.time), seconds));
    }

    @Override
    public void onChangeTryCount(int tryCount) {
        tvTryCount.setText(getString((R.string.try_count), tryCount));
    }

    private void showAlert(int message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setNeutralButton(R.string.ok, null)
                .setCancelable(false)
                .create()
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (game != null) {
            game.stopGame();
        }
    }
}
