package com.lightcoding.tokengame;

import android.content.Context;


/**
 * TokenGame
 * Created by Tomasz Poplawski on 06.06.2017.
 */

public class Token {

    TokenView tokenView;

    enum Side {
        COVERED, UNCOVERED
    }

    private final boolean isWinning;

    private Side side = Side.COVERED;

    public Side getSide() {
        return side;
    }

    public void setSide(Side side) {
        this.side = side;
        if (tokenView != null) {
            tokenView.updateView();
        }
    }

    public boolean isWinning() {
        return isWinning;
    }

    public Token(boolean isWinning) {
        this.isWinning = isWinning;
    }

    public TokenView getView(Context context) {
        return tokenView = new TokenView(context, this);
    }

}
