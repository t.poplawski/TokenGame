package com.lightcoding.tokengame;

/**
 * TokenGame
 * Created by Tomasz Poplawski on 06.06.2017.
 */

public interface TokenI {

    void onTokenClick(Token token);

}
