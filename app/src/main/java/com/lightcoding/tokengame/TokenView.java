package com.lightcoding.tokengame;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.view.ViewCompat;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatButton;
import android.view.ViewGroup;
import android.widget.TableRow;

/**
 * TokenGame
 * Created by Tomasz Poplawski on 06.06.2017.
 */

public class TokenView extends AppCompatButton {

    private ColorStateList colorUncovered;
    private ColorStateList colorWin;

    private final Token token;

    public TokenView(Context context, Token token) {
        super(context);
        this.token = token;
        setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1));
        colorUncovered = AppCompatResources.getColorStateList(getContext(), R.color.token_uncovered);
        colorWin = AppCompatResources.getColorStateList(getContext(), R.color.token_win);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    public Token getToken() {
        return token;
    }

    public void updateView() {

        if (token.isWinning()) {
            ViewCompat.setBackgroundTintList(this, colorWin);
        } else if (token.getSide() == Token.Side.UNCOVERED) {
            ViewCompat.setBackgroundTintList(this, colorUncovered);
        }

    }
}
