package com.lightcoding.tokengame;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * TokenGame
 * Created by Tomasz Poplawski on 07.06.2017.
 */
public class BoardTest implements TokenI {

    Board board;

    @Before
    public void setUp() throws Exception {
        board = new Board(Config.BOARD_WIDTH, Config.BOARD_HEIGHT, this);
    }

    @Test
    public void testHasOnlyOneWinToken() throws Exception {
        int tokens = 0;
        for (Token token : board.getTokens()) {
            if (token.isWinning()) {
                tokens += 1;
            }
        }

        assertEquals(tokens, 1);
    }

    @Test
    public void testIsAllTokensCovered() throws Exception {
        assertEquals(board.getTokens().length, board.getBoardWidth() * board.getBoardHeight());
        for (Token token : board.getTokens()) {
            assertEquals(token.getSide(), Token.Side.COVERED);
        }
    }

    @Override
    public void onTokenClick(Token token) {

    }
}