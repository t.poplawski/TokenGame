package com.lightcoding.tokengame;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * TokenGame
 * Created by Tomasz Poplawski on 06.06.2017.
 */
public class GameTest implements GameI {

    private Game game;
    private boolean win, loose;
    private int tryCount;

    @Before
    public void setUp() throws Exception {
        game = new Game(this);
    }

    @Test
    public void testStartGameStatus() throws Exception {
        assertEquals(game.getStatus(), Game.Status.IDLE);
        game.startGame();
        assertEquals(game.getStatus(), Game.Status.RUNNING);
    }

    @Test
    public void testStopGameStatus() throws Exception {
        game.startGame();
        game.stopGame();
        assertEquals(game.getStatus(), Game.Status.END);
    }

    @Test
    public void testIsBoardGenerated() throws Exception {
        assertNotEquals(game.getBoard(), null);
        assertEquals(game.getBoard().getTokens().length, game.getBoard().getBoardWidth() * game.getBoard().getBoardHeight());
        for (Token token : game.getBoard().getTokens()) {
            assertEquals(token.getSide(), Token.Side.COVERED);
        }
    }

    @Test
    public void testUseTokenTwoTimes() throws Exception {
        game.startGame();
        int randomIndexToken = new Random().nextInt(game.getBoard().getBoardWidth() * game.getBoard().getBoardHeight());
        Token token = game.getBoard().getTokens()[randomIndexToken];
        game.onTokenClick(token);
        game.onTokenClick(token);
        assertEquals(tryCount, 1);
    }

    @Test
    public void testWin() throws Exception {
        game.startGame();
        Token winToken = null;
        for (Token token : game.getBoard().getTokens()) {
            if (token.isWinning()) {
                winToken = token;
                break;
            }
        }

        assertNotNull(winToken);
        game.onTokenClick(winToken);
        assertEquals(win, true);
    }

    @Test
    public void testLoose() throws Exception {
        game.startGame();

        List<Token> tokens = new ArrayList<>();

        while (tokens.size() < Config.MAX_TRY) {
            Random generator = new Random();
            Token token = game.getBoard().getTokens()[generator.nextInt(Config.BOARD_WIDTH * Config.BOARD_HEIGHT)];
            if (!token.isWinning() && !tokens.contains(token)) {
                tokens.add(token);
                game.onTokenClick(token);
            }
        }

        assertEquals(loose, true);
    }


    @Override
    public void onEndTime() {
    }

    @Override
    public void onWin() {
        this.win = true;
    }

    @Override
    public void onLoose() {
        this.loose = true;
    }

    @Override
    public void onChangeTime(int seconds) {
    }

    @Override
    public void onChangeTryCount(int tryCount) {
        this.tryCount = tryCount;
    }
}